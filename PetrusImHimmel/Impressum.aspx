﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Impressum.aspx.cs" Inherits="PetrusImHimmel.Impressum"
MasterPageFile="~/MasterPage1.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
<style>
    dl, dd
    {
    	float:left;
    	text-align:left;    	
    }
    dt
    {
    	font-weight:bold;
    }
</style>
<dl>
<dt>Verantwortlich f&uuml;r diese Webseite: </dt>
<dd>Munir Husseini, Aachen, Deutschland <br />
    info(at)petrus-im-himmel.de</dd>
<dt>Danke an: </dt>
<dd>Ina Kl&ouml;ckner<br />Sina Gwosdzik<br />Thomas Stensitzki<br />iCOMcept GmbH</dd>
</dl>


</asp:Content>