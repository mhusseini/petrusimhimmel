﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.IO;
using iCOMcept.Web.Ajax;

namespace PetrusImHimmel
{
    [RegisterScript("js/LocationMap.js", RegisterScriptAttribute.RegistrationTypes.RegisterFromFileSystem)]
    public class LocationMap : AjaxControl
    {
        private ClientDictionary _items;
        private TextBox _textBox = new TextBox();
        private TextBox _txtSelectioName = new TextBox();
        private TextBox _txtSelectionLo = new TextBox();
        private TextBox _txtSelectionLa = new TextBox();

        public event EventHandler SelectionChanged;

        public int Selection { get { return GetValue(); } set { _textBox.Text = value.ToString(); } }
        public string SelectionName { get { return _txtSelectioName.Text; } set { _txtSelectioName.Text = value; } }
        public string SelectionLo { get { return _txtSelectionLo.Text; } set { _txtSelectionLo.Text = value; } }
        public string SelectionLa { get { return _txtSelectionLa.Text; } set { _txtSelectionLa.Text = value; } }

        public int Score { get; set; }
        public int CountryId { get; set; }
        public int Columns { get; set; }
        public int Rows { get; set; }
        public string Locale { get; set; }
        public string ImageUrl { get; set; }
        protected override HtmlTextWriterTag TagKey { get { return HtmlTextWriterTag.Div; } }
        protected override string TagName { get { return "div"; } }

        public LocationMap()
        {
            CountryId = 105;
            Columns = 10;
            Rows = 10;
            Score = 2;
            Locale = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;

            _textBox.Style.Add("display", "none");
            _textBox.ID = "textbox";
            _textBox.TextChanged += new EventHandler(OnSelectionChanged);

            _txtSelectioName.Style.Add("display", "none");
            _txtSelectioName.ID = "txtname";

            _txtSelectionLo.Style.Add("display", "none");
            _txtSelectionLo.ID = "txtlo";

            _txtSelectionLa.Style.Add("display", "none");
            _txtSelectionLa.ID = "txtla";

            Controls.Add(_textBox);
            Controls.Add(_txtSelectioName);
            Controls.Add(_txtSelectionLo);
            Controls.Add(_txtSelectionLa);
        }

        protected override void OnAddClientProperties(ClientObject o)
        {
            RenderJavaScript();
            base.OnAddClientProperties(o);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (DesignMode)
            {
                writer.Write(string.Format("<div style='border:solid 2px red;width:100%;height:100%;'>{0}</div>", ID));
            }
            else
            {
                base.RenderContents(writer);
            }
        }

        protected void RenderJavaScript()
        {
            using (SqlConnection cn = new SqlConnection(DataContext.CurrentDataContext.Connection.ConnectionString))
            using (SqlCommand cmd = cn.CreateCommand())
            {
                try
                {
                    cn.Open();

                    CreateCommandParameters(cmd);

                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        RenderJsObject(r);
                        
                        r.NextResult();

                        int index = 0;
                        while (r.Read())
                        {
                            RenderHotSpot(r, index++);
                        }
                    }
                }
                finally
                {
                    cn.Close();
                }
            }
        }

        private void RenderJsObject(IDataReader r)
        {
            _items = new ClientDictionary();

            ClientObject["items"] = _items;
            ClientObject["elementId"] = ClientID;
            ClientObject["screenId"] = string.Format("{0}_sc", ClientID);
            ClientObject["spotId"] = string.Format("{0}_sp", ClientID);
            ClientObject["imageId"] = string.Format("{0}_i", ClientID);
            ClientObject["selNameElementId"] = _txtSelectioName.ClientID;
            ClientObject["selLoElementId"] = _txtSelectionLo.ClientID;
            ClientObject["selLaElementId"] = _txtSelectionLa.ClientID;
            ClientObject["image"] = ImageUrl;
            ClientObject["rows"] = Rows;
            ClientObject["cols"] = Columns;
            ClientObject["formElementId"] = _textBox.ClientID;

            if (r.Read())
            {
                ClientObject["min_lo"] = (double)r["min_lo"];
                ClientObject["min_la"] = (double)r["min_la"];
                ClientObject["max_lo"] = (double)r["max_lo"];
                ClientObject["max_la"] = (double)r["max_la"];
            }
        }

        private void RenderHotSpot(IDataReader r, int index)
        {
            string name = r["Name"] as string;
            double lo = (double)r["normal_lo"];
            double la = (double)r["normal_la"];
            int id = (int)r["id"];

            ClientObject o = new ClientObject();
            _items[index] = o;

            o["name"] = name;
            o["lo"] = lo;
            o["la"] = la;
            o["id"] = id;
            o["radius"] = 1;

            if (Selection == id)
                ClientObject["selection"] = o;
        }

        private void CreateCommandParameters(SqlCommand cmd)
        {
            cmd.CommandText = "USP_Locations_SELECT_By_Grid";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@countryid", SqlDbType.Int).Value = CountryId;
            cmd.Parameters.Add("@cell_width", SqlDbType.Int).Value = Columns;
            cmd.Parameters.Add("@cell_height", SqlDbType.Int).Value = Rows;
            cmd.Parameters.Add("@locale", SqlDbType.NVarChar).Value = Locale;
            cmd.Parameters.Add("@score", SqlDbType.Int).Value = Score;
        }

        void OnSelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, e);
            }
        }

        private int GetValue()
        {
            int i;
            int.TryParse(_textBox.Text, out i);
            return i;
        }
    }
}
