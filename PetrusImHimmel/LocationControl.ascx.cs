﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PetrusImHimmel
{
    public partial class LocationControl : System.Web.UI.UserControl
    {
        public String LocationMapId { get; set; }
        public int SelectedLocation { get { return LocationMap1.Selection; } }

        protected override void OnPreRender(EventArgs e)
        {
            AutoCompleteExtender1.OnClientItemSelected = string.Format("function(){{mkh_Locations_getLocationByName({0}, $(\"#{1}\"));}}", LocationMap1.ClientObject.ClientID , txtLocation.ClientID);
            txtLocation.Text = string.Empty;
            base.OnPreRender(e);
        }

        protected void LocationMap1_SelectionChanged(object sender, EventArgs e)
        {
            DataContext.CurrentWeatherRequest.LocationID = LocationMap1.Selection;
        }
    }
}