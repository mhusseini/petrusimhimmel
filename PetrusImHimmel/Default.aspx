<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage1.master"
    CodeBehind="Default.aspx.cs" Inherits="PetrusImHimmel._Default" %>

<%@ Register Src="WeatherDataControl.ascx" TagName="WeatherDataControl" TagPrefix="uc1" %>
<%@ Register Src="LocationControl.ascx" TagName="LocationControl" TagPrefix="uc2" %>
<%@ Register Src="DateRangeControl.ascx" TagName="DateRangeControl" TagPrefix="uc3" %>
<%@ Register Src="TextControl.ascx" TagName="TextControl" TagPrefix="uc4" %>
<%@ Register Assembly="PetrusImHimmel" Namespace="PetrusImHimmel" TagPrefix="pih" %>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td rowspan="2" valign="bottom">
                <uc1:WeatherDataControl ID="WeatherDataControl1" runat="server" />
            </td>
            <td class="textTd">
                <p id="intro">
                    <uc4:TextControl ID="TextControl1" runat="server" TextName="Welcome" />
                </p>
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Absenden" />
            </td>
            <td rowspan="2" valign="bottom">
                <uc2:LocationControl ID="LocationControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="center" valign="bottom">
                <uc3:DateRangeControl ID="DateRangeControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div id="debug">
    </div>
</asp:Content>
