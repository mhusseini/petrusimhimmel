﻿function setup_Slider(o)
{
    var c = o.cssClass;
    o.element = $('#' + o.elementId);
    
    o.element.append(
    '<div class="'+c+' header">' +
        '<div id="' + o.captionElementId  + '" class="'+c+' caption"><span></span></div>' +
        '<div id="' + o.valueElementId  + '" class="'+c+' value"><span></span></div>' +
    '</div>' +
    '<div id="' + o.imageElementId  + '" class="'+c+' image">' +
        '<div id="' + o.handleElementId  + '" class="'+c+' handle"></div>' + 
    '</div>'
    );
    
    o.formElement = $('#' + o.formElementId);
    o.captionElement = $('#' + o.captionElementId);
    o.valueElement = $('#' + o.valueElementId);
    o.imageElement = $('#' + o.imageElementId);
    o.handleElement = $('#' + o.handleElementId);
    
    o.captionElement.find("span").text(o.caption);    
    o.captionElement.text(" ");    
    o.lastMousePageX = -99999;
    o.lastMousePageY = -99999;
    o.mousePressed = false;
    
    mkh_sld_applyEvents(o.imageElement, o);
    mkh_sld_applyEvents(o.handleElement, o);
    
    mkh_sld_displayvalue(o);
    mkh_sld_positionHandle(o);
}

function mkh_sld_applyEvents(el, o)
{
    el.mousemove(
        function (e) { mkh_sld_mousemove(e, o, this); }
    );
    
    el.mousedown(
        function (e) { o.mousePressed = true; 
        mkh_sld_mousemove(e, o, this); }
    );
    
    el.mouseup(
        function (e) { o.mousePressed = false; }
    );
}

function mkh_sld_mousemove(e, o, el)
{   	
    if(!o.mousePressed) return;
    
    var pos = mkh_GetRelativeCursorPosition(e, o.imageElement, o);
    
    if(!pos.isInside) 
    {
        o.mousePressed = false;
        return;
    }
    
    var rw = o.handleElement.width() / 2;
    
    if(pos.x < rw || pos.x >= o.imageElement.width() - rw) return;
    
    //
    // position handle
    //
    o.handleElement.css("left", (pos.x - rw) );
    
    //
    // get value
    //
    var v = pos.x / (o.imageElement.width() - 2 * rw) * (o.maximum - o.minimum) + o.minimum;
    
    if(v > o.maximum) v = o.maximum;
    else if(v < o.minimum) v = o.minimum;
    
    //
    // save value
    //
    o.value = v;    
    o.formElement.val(v);
    
    //
    // write value
    //
    mkh_sld_displayvalue(o, v);
}

function mkh_sld_displayvalue(o)
{
    var t;
    var v = Math.round(o.value);
    
    t = o.names.length > 0 && o.names[v] ?
        o.names[v] : 
        v.numberFormat(o.formatString);
        
    v = v - o.minimum;
    if(v >= 0)
    {
        var d = v * o.valueElement.height();
        o.valueElement.css('background-position', '0px -' + d + 'px');
    }
    
    o.valueElement.find("span").text(t);  
}

function mkh_sld_positionHandle(o)
{
    var rw = o.handleElement.width() / 2;
    var x = (o.value - o.minimum) / (o.maximum - o.minimum)  * (o.imageElement.width() - 2 * rw) - rw;
    
    o.handleElement.css("left", x);
}