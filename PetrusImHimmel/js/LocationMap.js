﻿function setup_LocationMap(o)
{
    var el = $('#' + o.elementId);
    o.element = el;

    el.append('<img id="' + o.imageId  + '" class="image" style="width:100%;height:100%;" src="' + o.image + '" />');
    el.append('<table id="' + o.screenId +   '" class="screen"><tr><td></td></tr></table>');
    el.append('<table id="' + o.spotId +   '" class="hotspot"><tr><td></td></tr></table>');
    
    o.imageElement = $('#' + o.imageId);
    o.screenElement = $('#' + o.screenId);
    o.spotElement = $('#' + o.spotId);
    o.formElement = $('#' + o.formElementId);
    o.selNameElement = $('#' + o.selNameElementId);
    o.selLoElement = $('#' + o.selLoElementId);
    o.selLaElement = $('#' + o.selLaElementId);
    o.text = "";
    o.radius = Math.max( el.width() / o.cols, el.height() / o.rows ) * Math.sqrt(0.5);

    o.lastMousePageX = -9999999;
    o.lastMousePageY = -9999999;
    
    for(var i = 0; i < o.items.length; i++)
    {
        var item = o.items[i];
        item.x = Math.round(item.lo * o.element.width());
        item.y = Math.round(item.la * o.element.height());
    }
    
    mkh_mlp_applyEvents(o.imageElement, o);
    mkh_mlp_applyEvents(o.spotElement, o);
    mkh_mlp_applyEvents(o.element, o);
    
    mkh_mlp_setValue(o);
    
    el.blur(
        function () { o.spotElement.hide(); }
    );
}

function mkh_mlp_applyEvents(el, o)
{
    el.mousemove(
        function (e) { mkh_mlp_mousemove(e, o, this); }
    );
    
    el.click( 
        function(e) { mkh_mlp_click(e,o,this); }
    );
}

function mkh_mlp_mousemove(e, o, el)
{    
    var pos = mkh_GetRelativeCursorPosition(e, o.element, o);
    
    if(!pos.isInside)
    {
        o.spotElement.hide();
        return;
    }
    
    var x = pos.x;
    var y = pos.y;
    
    var text = ""; // x + ', ' + y;
    o.selection = null;
    
    for(var i = 0; i < o.items.length; i++)
    {
        var item = o.items[i];
        var r = item.radius * o.radius;
        
        if( x >= item.x - r && y >= item.y -r &&
            x < item.x + r && y < item.y  + r )
        {
            text = item.name;
            o.selection = item;
            break;
        }
    }
    
    if(o.text != text)
    {
        o.text = text;
        o.spotElement.find('td').text(text);
        
        if(text.length == 0)
            o.spotElement.attr('class', 'hotspot');
        else
            o.spotElement.addClass('selected_hotspot');
    }
        
    mkh_mlp_showSpot(o, x, y);
}

function mkh_mlp_click(e, o, el)
{
    if(o.selection == null || o.value == o.selection.id)
    {
        return;
    }
    
    o.value = o.selection.id;
    o.selNameElement.val('');
    o.selLaElement.val('');
    o.selLoElement.val('');
    
    mkh_mlp_setValue(o);
}

function mkh_mlp_showSpot(o, x, y)
{        
    o.spotElement.css("cursor", o.text.length == 0 ? "default" : "pointer" );
    mkh_mlp_place(o, x, y, o.spotElement);
}

function mkh_mlp_setValue(o)
{    
    var name;
    var x;
    var y;
    
    if(o.selection)
    {
        name = o.selection.name;
        x = o.selection.x;
        y = o.selection.y;
    }
    else
    {
        name = o.selNameElement.val();
        x = o.selLoElement.val();
        y = o.selLaElement.val();
        
        x = (x - o.min_lo) / (o.max_lo - o.min_lo) * o.element.width();
	    y = ( 1 - (y - o.min_la) / (o.max_la - o.min_la) ) * o.element.height();
    }
    
    if( name && x && y)
    {    
        o.formElement.val(o.value);
        o.screenElement.find('td').text(name);
        
        mkh_mlp_place(o, x, y, o.screenElement);
    }
}

function mkh_mlp_place(o, x, y, el)
{    
    el.css("left", (x - el.width() / 2) );
    el.css("top", (y - el.height() / 2) );
//    
    el.show();
}

function mkh_mlp_setExtraValue(o, id, name, lo, la)
{   
    o.selection = null;
    o.value = id;
	o.selNameElement.val(name);
	o.selLoElement.val(lo);
	o.selLaElement.val(la);
	
	mkh_mlp_setValue(o);
}