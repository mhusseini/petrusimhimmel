﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Globalization;
using iCOMcept.Web.Ajax;

namespace PetrusImHimmel
{
    [RegisterScript("js/Slider.js", RegisterScriptAttribute.RegistrationTypes.RegisterFromFileSystem)]
    [RegisterScript("js/number-functions.js", RegisterScriptAttribute.RegistrationTypes.RegisterFromFileSystem)]
    public class Slider : AjaxControl
    {
        private TextBox txtValue = new TextBox() { ID = "txtvalue" };

        public event EventHandler ValueChanged;
        public Dictionary<int, string> ValueNames { get; set; }


        private static NumberFormatInfo _NumberFormat;
        private static NumberFormatInfo NumberFormat
        {
            get
            {
                if (_NumberFormat == null)
                {
                    _NumberFormat = CultureInfo.GetCultureInfo("en-US").NumberFormat;
                }

                return _NumberFormat;
            }
        }
        public object Tag { get; set; }
        public double Value { get { return GetValue(); } set { txtValue.Text = value.ToString(NumberFormat); } }
        public double Maximum { get; set; }
        public double Minimum { get; set; }
        public string FormatString { get; set; }
        public string Caption { get; set; }

        protected override HtmlTextWriterTag TagKey { get { return HtmlTextWriterTag.Div; } }
        protected override string TagName { get { return "div"; } }

        public Slider()
        {
            FormatString = string.Empty;
            Maximum = 0;
            Maximum = 10;
            Value = 0;

            txtValue.TextChanged += new EventHandler(OnValueChanged);
            txtValue.Style.Add("display", "none");
            Controls.Add(txtValue);
        }

        protected override void OnInit(EventArgs e)
        {
            // we need this for formatting numbers
            //RegisterScript("js/number-functions.js", false);

            base.OnInit(e);
        }

        private double GetValue()
        {
            
            double i = 0;
            double.TryParse(txtValue.Text, NumberStyles.Number ^ NumberStyles.AllowThousands, NumberFormat, out i);
            return i;
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
        }

        protected override void OnAddClientProperties(ClientObject o)
        {
            ClientObject["captionElementId"] = string.Format("{0}_c", ClientID);
            ClientObject["valueElementId"] = string.Format("{0}_v", ClientID);
            ClientObject["formElementId"] = txtValue.ClientID;
            ClientObject["imageElementId"] = string.Format("{0}_i", ClientID);
            ClientObject["handleElementId"] = string.Format("{0}_h", ClientID);
            ClientObject["elementId"] = ClientID;
            ClientObject["cssClass"] = CssClass;
            ClientObject["formatString"] = FormatString;
            ClientObject["caption"] = Caption;
            ClientObject["minimum"] = Minimum;
            ClientObject["maximum"] = Maximum;
            ClientObject["value"] = Value;

            ClientDictionary csd = new ClientDictionary();
            ClientObject["names"] = csd;

            foreach (KeyValuePair<int, string> kvp in ValueNames)
            {
                csd[kvp.Key] = kvp.Value;
            }

            base.OnAddClientProperties(o);
        }

        void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }
    }
}
