﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tables.aspx.cs" Inherits="PetrusImHimmel.Admin.Tables" %>

<%@ Register assembly="RadGrid.Net2" namespace="Telerik.WebControls" tagprefix="rad" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    
        <rad:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" 
            GridLines="None">
<MasterTableView autogeneratecolumns="False" datakeynames="RequestID" 
                datasourceid="SqlDataSource1">
<RowIndicatorColumn Visible="False">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="False" Resizable="False">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <rad:GridBoundColumn DataField="RequestID" DataType="System.Int32" 
            HeaderText="RequestID" ReadOnly="True" SortExpression="RequestID" 
            UniqueName="RequestID">
        </rad:GridBoundColumn>
        <rad:GridBoundColumn DataField="CreateDate" DataType="System.DateTime" 
            HeaderText="CreateDate" SortExpression="CreateDate" UniqueName="CreateDate">
        </rad:GridBoundColumn>
        <rad:GridBoundColumn DataField="BeginDate" DataType="System.DateTime" 
            HeaderText="BeginDate" SortExpression="BeginDate" UniqueName="BeginDate">
        </rad:GridBoundColumn>
        <rad:GridBoundColumn DataField="EndDate" DataType="System.DateTime" 
            HeaderText="EndDate" SortExpression="EndDate" UniqueName="EndDate">
        </rad:GridBoundColumn>
        <rad:GridBoundColumn DataField="Clouds" DataType="System.Decimal" 
            HeaderText="Clouds" SortExpression="Clouds" UniqueName="Clouds">
        </rad:GridBoundColumn>
        <rad:GridBoundColumn DataField="Rain" DataType="System.Decimal" 
            HeaderText="Rain" SortExpression="Rain" UniqueName="Rain">
        </rad:GridBoundColumn>
        <rad:GridBoundColumn DataField="Temprature" DataType="System.Decimal" 
            HeaderText="Temprature" SortExpression="Temprature" UniqueName="Temprature">
        </rad:GridBoundColumn>
        <rad:GridBoundColumn DataField="Storm" DataType="System.Decimal" 
            HeaderText="Storm" SortExpression="Storm" UniqueName="Storm">
        </rad:GridBoundColumn>
    </Columns>
</MasterTableView>
        </rad:RadGrid>
    
    
    </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
    SelectCommand="
    SELECT DISTINCT wr.RequestID, wr.CreateDate,  wr.BeginDate, wr.EndDate, 
    sv1.Value AS Clouds, sv2.Value AS Rain, sv3.Value AS Temprature, sv4.Value AS Storm
    FROM WeatherRequest wr

    INNER JOIN ScaleValues sv1 ON wr.RequestID = sv1.RequestID AND sv1.ScaleID = 1
    INNER JOIN ScaleValues sv2 ON wr.RequestID = sv2.RequestID AND sv2.ScaleID = 2
    INNER JOIN ScaleValues sv3 ON wr.RequestID = sv3.RequestID AND sv3.ScaleID = 3
    INNER JOIN ScaleValues sv4 ON wr.RequestID = sv4.RequestID AND sv4.ScaleID = 5

    ORDER BY wr.CreateDate
    " ConnectionString="<%$ ConnectionStrings:PetrusImHimmelConnectionString2 %>"></asp:SqlDataSource>
    </form>
</body>
</html>
