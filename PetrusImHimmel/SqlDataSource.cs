﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Data;
using System.Web.UI;

namespace PetrusImHimmel
{
    public class SqlDataSource : System.Web.UI.WebControls.SqlDataSource
    {
        protected override void OnInit(EventArgs e)
        {
            ConnectionString = DataContext.CurrentDataContext.Connection.ConnectionString;
            base.OnInit(e);
        }

        public DataView SelectDataView()
        {
            return Select(new DataSourceSelectArguments()) as DataView;
        }

        public DataRowView SelectDataRowView()
        {
            DataRowView result = null;
            DataView dv = SelectDataView();

            if (dv != null && dv.Count > 0)
            {
                result = dv[0];
            }

            return result;
        }
    }
}
