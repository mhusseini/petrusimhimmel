﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace PetrusImHimmel
{
    public partial class WeatherDataControl : System.Web.UI.UserControl
    {
        protected WeatherRequest WR { get { return DataContext.CurrentWeatherRequest; } }

        protected override void OnLoad(EventArgs e)
        {
            Repeater1.DataSource = from o in DataContext.CurrentDataContext.Scales select o;
            base.OnLoad(e);
        }

        protected ScaleString GetScaleString(object s)
        {
            Scale scale = s as Scale;

            return (from o in scale.ScaleStrings
                    where o.Locale == CultureInfo.CurrentCulture.Name.ToLower() &&
                    o.ScaleID == scale.ScaleID
                    select o).FirstOrDefault();
        }

        protected ScaleValue GetValue(object s)
        {
            Scale scale = s as Scale;

            WeatherRequest wr = DataContext.CurrentWeatherRequest;

            return (from o in wr.ScaleValues
                            where o.ScaleID == scale.ScaleID
                            select o).FirstOrDefault();
        }

        protected Dictionary<int, string> GetValueNames(object s)
        {
            Scale scale = s as Scale;

            return scale.ScaleValueNames.ToDictionary(sc => sc.ScaleValue, sc => sc.Name);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        protected void OnScaleValueChanged(object sender, EventArgs e)
        {
            Slider slider = sender as Slider;
            IDataItemContainer dic = slider.NamingContainer as IDataItemContainer;
            ScaleValue v = DataContext.CurrentWeatherRequest.ScaleValues[dic.DataItemIndex];
            v.Value = (decimal)slider.Value;
        }
    }
}