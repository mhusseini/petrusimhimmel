using System.Web;
namespace PetrusImHimmel
{
    partial class DataContext
    {
        private static readonly string _requestSessionKey = "PetrusImHimmel.DataContext.CurrentWeatherRequest";

        public static WeatherRequest CurrentWeatherRequest
        {
            get
            {
                if (HttpContext.Current.Session[_requestSessionKey] is WeatherRequest)
                    return HttpContext.Current.Session[_requestSessionKey] as WeatherRequest;
                else
                    return CreateCurrentWeatherRequest();
            }
        }

        private static WeatherRequest CreateCurrentWeatherRequest()
        {
            DataContext ctx = DataContext.CurrentDataContext;

            WeatherRequest wr = new WeatherRequest
            {
                CreateDate = DateTime.Now,
#warning check this
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now,
                DeleteDate = DateTime.Now.AddMonths(12),
                LocationID = 41 // Germany
            };

            foreach (var scale in from o in ctx.Scales select o)
            {
                wr.ScaleValues.Add(new ScaleValue
                {
                    Scale = scale,
                    Value = scale.DefaultValue
                });
            }

            ctx.WeatherRequests.InsertOnSubmit(wr);
            ctx.ScaleValues.InsertAllOnSubmit(wr.ScaleValues);

            HttpContext.Current.Session[_requestSessionKey] = wr;

            return wr;
        }

        public static DataContext CurrentDataContext
        {
            get
            {
                DataContext ctx = null;

                if (HttpContext.Current.Session["PetrusImHimmel.DataContext"] is DataContext)
                {
                    ctx = HttpContext.Current.Session["PetrusImHimmel.DataContext"] as DataContext;
                }
                else
                {
                    ctx = new DataContext();
                    HttpContext.Current.Session["PetrusImHimmel.DataContext"] = ctx;
                }

                //ctx = new DataContext();

                return ctx;
            }
        }

        public static void Save()
        {
            CurrentDataContext.SubmitChanges();
        }
    }
}
