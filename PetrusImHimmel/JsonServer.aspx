﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JsonServer.aspx.cs" Inherits="PetrusImHimmel.JsonServer" %>

<%@ Register Assembly="iCOMcept.Web.Ajax" Namespace="iCOMcept.Web.Ajax" TagPrefix="cc1" %>
<%@ Register Assembly="PetrusImHimmel" Namespace="PetrusImHimmel" TagPrefix="pih" %>
<cc1:JsonResponder ID="srvLocationNames" runat="server" AutoHandleQuery="false" DataSourceID="sqlNames">
</cc1:JsonResponder>
<cc1:JsonResponder ID="srvLocationData" runat="server" AutoHandleQuery="false" DataSourceID="sqlData">
</cc1:JsonResponder>

<pih:SqlDataSource id="sqlNames" runat="server" selectcommand="
    SELECT TOP 10 Name, 
    CASE WHEN zipstart = zipend THEN zipstart ELSE zipstart + ' - ' + zipend END AS Zip
    FROM Locations 
    WHERE name LIKE @queryString + '%' AND 
        country=105 AND 
        NOT zipstart IS NULL 
        AND NOT population IS NULL
    ORDER BY population desc">
    <SelectParameters>
        <asp:FormParameter name="queryString" formfield="queryString" />
    </SelectParameters>
</pih:SqlDataSource>

<pih:SqlDataSource id="sqlData" runat="server" selectcommand="
    exec USP_Locations_SELECT_By_Name @name = @queryString">
    <SelectParameters>
        <asp:FormParameter name="queryString" formfield="queryString" />
    </SelectParameters>
</pih:SqlDataSource>
