﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PetrusImHimmel
{
    public partial class JsonServer : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            switch (Request["action"])
            {
                case "data":
                    srvLocationData.HandleQuery();
                    break;
                default:
                    srvLocationNames.HandleQuery();
                    break;
            }

            base.OnInit(e);
        }
    }
}
