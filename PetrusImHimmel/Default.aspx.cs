using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PetrusImHimmel
{
    public partial class _Default : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {

            DataBind();
            base.OnPreRender(e);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DataContext.CurrentWeatherRequest.LocationID = LocationControl1.SelectedLocation;
            DataContext.Save();
            Response.Redirect("SubmitFeedback.aspx");
        }
    }
}
