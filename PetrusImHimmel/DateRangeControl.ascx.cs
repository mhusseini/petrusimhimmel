﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PetrusImHimmel
{
    public partial class DateRangeControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            txtFrom.Text = DataContext.CurrentWeatherRequest.BeginDate.ToString("d");
            txtTo.Text = DataContext.CurrentWeatherRequest.EndDate.ToString("d");
            base.OnPreRender(e);
        }

        protected void txtFrom_TextChanged(object sender, EventArgs e)
        {
            DateTime dt;
            if (DateTime.TryParse(txtFrom.Text, out dt))
            {
                DataContext.CurrentWeatherRequest.BeginDate = dt;
            }
        }

        protected void txtTo_TextChanged(object sender, EventArgs e)
        {
            DateTime dt;
            if (DateTime.TryParse(txtFrom.Text, out dt))
            {
                DataContext.CurrentWeatherRequest.EndDate = dt;
            }
        }
    }
}