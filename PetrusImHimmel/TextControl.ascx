﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextControl.ascx.cs" Inherits="PetrusImHimmel.TextControl" %>

<%@ Register Assembly="PetrusImHimmel" Namespace="PetrusImHimmel" TagPrefix="pih" %>

<asp:Label ID="Label1" runat="server"></asp:Label>

<pih:SqlDataSource ID="SqlDataSource1" runat="server" 
    SelectCommand="
    SELECT TOP 1 Text FROM Texts
    WHERE Name = @TextName
    ORDER BY NEWID()
    ">
    <SelectParameters>
    <asp:Parameter Name="TextName" DbType="String" />
    </SelectParameters>
</pih:SqlDataSource>
