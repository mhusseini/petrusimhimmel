﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WeatherDataControl.ascx.cs"
    Inherits="PetrusImHimmel.WeatherDataControl" %>
<%@ Register Assembly="PetrusImHimmel" Namespace="PetrusImHimmel" TagPrefix="cc1" %>

<asp:Panel runat="server" ID="pnlScales" CssClass="weather">
    <asp:Repeater ID="Repeater1" runat="server">
        <ItemTemplate>
            <cc1:Slider ID="Slider1" runat="server" CssClass='<%# (Container.DataItem as Scale).Name.ToLower() + " scale" %>'
                FormatString='<%# GetScaleString(Container.DataItem).StringFormat %>' 
                Caption='<%# GetScaleString(Container.DataItem).Name + ":" %>'
                Minimum='<%# (Container.DataItem as Scale).Minimum %>'
                Maximum='<%# (Container.DataItem as Scale).Maximum %>'
                Value='<%# GetValue(Container.DataItem).Value %>'
                Tag='<%# GetValue(Container.DataItem) %>'
                ValueNames='<%# GetValueNames(Container.DataItem) %>'
                OnValueChanged='OnScaleValueChanged'
                />
                
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
