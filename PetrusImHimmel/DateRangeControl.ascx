﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateRangeControl.ascx.cs" Inherits="PetrusImHimmel.DateRangeControl" %>

<link rel="stylesheet" href="style/ui.datepicker.css" type="text/css" />
		
<asp:Panel runat="server" CssClass="dateRange">
<span>Von:</span>
<asp:TextBox class="dateRangeTextBox1" runat="server" ID="txtFrom" 
    ontextchanged="txtFrom_TextChanged"></asp:TextBox>
<span>Bis:</span>
<asp:TextBox class="dateRangeTextBox2" runat="server" ID="txtTo" 
    ontextchanged="txtTo_TextChanged"></asp:TextBox>

		<!-- Include Core Datepicker JavaScript -->
		<script src="js/ui.datepicker.js" type="text/javascript" charset="utf-8"></script>	
		
		<!-- Attach the datepicker to dateinput after document is ready -->
		<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('.dateRangeTextBox1').datepicker({beforeShow: customRange, dateFormat: 'dd.mm.yy'});
			$('.dateRangeTextBox2').datepicker({beforeShow: customRange, dateFormat: 'dd.mm.yy'});
			// Customize two date pickers to work as a date range
        
            function customRange(input) 
            {                 
                return { 
                    minDate: (input.className == 'dateRangeTextBox1' ? $('.dateRangeTextBox1').datepicker('getDate') : null),
                    maxDate: (input.className == 'dateRangeTextBox2' ? $('.dateRangeTextBox2').datepicker('getDate') : null)
                }
            }
        });
		</script>
		
</asp:Panel>