﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PetrusImHimmel
{
    public partial class TextControl : System.Web.UI.UserControl
    {
        public string TextName { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            SqlDataSource1.SelectParameters["TextName"].DefaultValue = TextName;
            Label1.Text = SqlDataSource1.SelectDataRowView()[0] as string;

            base.OnLoad(e);
        }
    }
}