﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationControl.ascx.cs"
    Inherits="PetrusImHimmel.LocationControl" %>
<%@ Register Assembly="PetrusImHimmel" Namespace="PetrusImHimmel" TagPrefix="cc1" %>
<%@ Register Assembly="iCOMcept.Web.Ajax" Namespace="iCOMcept.Web.Ajax" TagPrefix="icc" %>

<script language="javascript" type="text/javascript">
    function mkh_Locations_getLocationByName(o, el) {
        var v = el.valueOf();
        if (!v || v.length == 0) return;

        $.post("JsonServer.aspx",
            { queryString: "" + el.val() + "", action:'data' },
            function(result) {
                el.val("");
                if (result.Data.length > 0)
                    mkh_mlp_setExtraValue(o, result.Data[0][0], result.Data[0][1], result.Data[0][2], result.Data[0][3]);
            },
            "json"
            );
        }

        function mkh_Locations_txtLocation_onKeyPress(event) {
            var v = event.charCode ? event.charCode : event.keyCode;
            var dbo = $('.autocomplete.cbo:visible');
            if (v == 13) {
                if (dbo.length > 0) { 
                return false; }
                else {
                mkh_Locations_getLocationByName(<%=LocationMap1.ClientObject.ClientID %>, $("#<%=txtLocation.ClientID %>"));
                }
            }

            return true;
        }
</script>

<asp:Panel ID="Panel1" runat="server" CssClass="Location">
    <div class="autocomplete" id="Location">
        <asp:TextBox ID="txtLocation" runat="server" onkeypress="return mkh_Locations_txtLocation_onKeyPress(event)">
        </asp:TextBox>
        <icc:AutoCompleteExtender ID="AutoCompleteExtender1" CssClass="autocomplete cbo"
            runat="server" ExtendedControlID="txtLocation" PostbackUrl="JsonServer.aspx">
            <TargetElements>
                <icc:ElementReference RefID="txtLocation" />
            </TargetElements>
        </icc:AutoCompleteExtender>
    </div>
    <a id="btnGetLocationData" style="cursor: pointer;" onclick='mkh_Locations_getLocationByName(<%=LocationMap1.ClientObject.ClientID %>, $("#<%=txtLocation.ClientID %>"))'>
        &nbsp;</a>
    <cc1:LocationMap ID="LocationMap1" runat="server" ImageUrl="images/germany2.png"
        SelectionName='<%=DataContext.CurrentWeatherRequest.Location.Name %>' SelectionLo='<%=DataContext.CurrentWeatherRequest.Location.Longitude %>'
        SelectionLa='<%=DataContext.CurrentWeatherRequest.Location.Latitude %>' OnSelectionChanged="LocationMap1_SelectionChanged" />
</asp:Panel>
